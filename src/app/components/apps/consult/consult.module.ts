import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { SharedModule } from '../../../shared/sharedModule';

import { ConsultRoutingModule } from './consult-routing.module';
import { ConsultComponent } from './consult.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { ChipModule } from 'primeng/chip';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { CardModule } from 'primeng/card';
import { SpeedDialModule } from 'primeng/speeddial';
import { AvatarModule } from 'primeng/avatar';

@NgModule({
  declarations: [
    ConsultComponent
  ],
  imports: [
    CommonModule,
    ConsultRoutingModule,
    NgxSpinnerModule,
    ButtonModule,
    ChipModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    DropdownModule,
    CardModule,
    SpeedDialModule,
    AvatarModule
  ]
})
export class ConsultModule { }
