import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmissionNoteComponent } from './admission-note.component';

describe('AdmissionNoteComponent', () => {
  let component: AdmissionNoteComponent;
  let fixture: ComponentFixture<AdmissionNoteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdmissionNoteComponent]
    });
    fixture = TestBed.createComponent(AdmissionNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
