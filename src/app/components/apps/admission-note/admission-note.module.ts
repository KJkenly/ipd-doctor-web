import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from "ngx-spinner";
import { AdmissionNoteRoutingModule } from './admission-note-routing.module';
import { AdmissionnoteComponent} from './admission-note.component' 
import {SharedModule} from '../../../shared/sharedModule';

import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { PanelModule } from 'primeng/panel';
import { SplitterModule } from 'primeng/splitter';
import { CheckboxModule } from 'primeng/checkbox';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SpeedDialModule } from 'primeng/speeddial';

@NgModule({
  declarations: [
    AdmissionnoteComponent
  ],
  imports: [
     CommonModule,
        FormsModule,
    AdmissionNoteRoutingModule,NgxSpinnerModule,MessagesModule,MessageModule,SharedModule,PanelModule,SplitterModule,CheckboxModule,RadioButtonModule,
    TableModule,InputTextModule,ButtonModule,InputTextareaModule,CalendarModule,SpeedDialModule
  ]
})
export class AdmissionNoteModule { }
