import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AvatarModule } from 'primeng/avatar';
import { AvatarGroupModule } from 'primeng/avatargroup';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ButtonModule } from 'primeng/button';
import { ChipModule } from 'primeng/chip';
import { RippleModule } from 'primeng/ripple';
import { SidebarModule } from 'primeng/sidebar';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { DividerModule } from 'primeng/divider';
import { SharedModule } from '../../../shared/sharedModule';

import { DoctorOrderRoutingModule } from './doctor-order-routing.module';
import { DoctorOrderComponent } from './doctor-order.component';
import { ProgressNoteComponent } from './progress-note/progress-note.component';
import { OrderContinueComponent } from './order-continue/order-continue.component';
import { OrderOnedayComponent } from './order-oneday/order-oneday.component';

@NgModule({
    declarations: [
        DoctorOrderComponent,
        ProgressNoteComponent,
        OrderContinueComponent,
        OrderOnedayComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,ReactiveFormsModule,
        NgxSpinnerModule,
        DoctorOrderRoutingModule,
        AvatarModule,
        AvatarGroupModule,
        MessagesModule,
        MessageModule,
        ButtonModule,
        ChipModule,
        RippleModule,
        SidebarModule,
        AutoCompleteModule,
        ProgressSpinnerModule,
        DividerModule,
        SharedModule
    ],
})
export class DoctorOrderModule {}
