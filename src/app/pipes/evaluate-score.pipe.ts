import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'evaluateScore'
})

export class EvaluateScorePipe implements PipeTransform {

    transform(score: string, format: string): string {
        let tmp_score = parseInt(score);
        let text_result : any;      
        if(format == 'barthel'){
            if(tmp_score <= 4) {
                text_result = 'ภาวะพึ่งพาโดยสมบูรณ์: very low initial score, total dependence' ;
            } else if(tmp_score >= 5 && tmp_score <= 8) {
                text_result = 'ภาวะพึ่งพารุนแรง : low initial score, severe dependence' ;
            } else if(tmp_score >= 9 && tmp_score <= 11) {
                text_result = 'ภาวะพึ่งพาปานกลาง :  intermediate initial score, moderately severs dependence' ;
            } else if(tmp_score >= 13 && tmp_score <= 20) {
                text_result = 'ไม่เป็นการพึ่งพา : intermediate high, mildly severs dependence, consideration of discharging home' ;
            } else {
                text_result = '-' ;
            }
        }else if(format == 'adl'){
            if(tmp_score <= 4) {
                text_result = 'ช่วยเหลือตัวเองไม่ได้ (Totally dependent)' ;
            }
            else if(tmp_score >= 5 && tmp_score <= 8) {
                text_result = 'ช่วยแหลือและดูแลตนเองได้บ้าง (Partially dependent)' ;
            }
            else if(tmp_score >= 9) {
                text_result = 'ช่วยเหลือตัวเองได้ (Independent)' ;
            }
            else {
                text_result = '-' ;
            }
        }else if(format == 'screen'){
            if(tmp_score >= 0 && tmp_score <= 1) {
                text_result = 'ปกติ' ;
            }
            else if(tmp_score >= 2) {
                text_result = 'ผิดปกติ' ;
            }
            else {
                text_result = '-' ;
            }
        }else if(format == 'esas'){
        }else if(format == 'psyche'){
            if(tmp_score > 9 ){
                text_result = 'ให้ส่งต่อไปรับการประเมินและรักษาที่โรงพยาบาล' ;
            } else {
                text_result = 'รักษาตามระดับความรุนแรง'
            }
        }else if(format == 'nutrition'){
            if(tmp_score <= 5) {
                text_result = 'ไม่พบความเสี่ยงต่อการเกิดภาวะทุพโภชนาการ' ;
            }
            else if(tmp_score >= 6 && tmp_score <= 10) {
                text_result = 'พบความเสี่ยงต่อการเกิดภาวะโภชนาการ ควรปรึกษานักโภชนาการ' ;
            }
            else if(tmp_score >= 11) {
                text_result = 'มีภาวะทุพโภชนาการ ควรปรึกษานักโภชนาการ และปรึกษาแพทย์' ;
            }            
            else {
                text_result = '-' ;
            }
        }else if(format == 'violent'){
            if(tmp_score == 3) {
                text_result = 'ผู้ป่วยมีพฤติกรรมก้าวร้าวรุนแรง ที่ไม่สามารถควบคุมตนเองได้ จนเกิดอันตรายต่อตนเอง หรือผู้อื่น หรือทรัพย์สิน ต้องจัดการทันทีทันใด' ;
            }
            else if(tmp_score == 2 ) {
                text_result = 'ผู้ป่วยมีพฤติกรรมก้าวร้าวรุนแรง ที่เริ่มควบคุมตนเองไม่ได้ มีท่าทีที่อาจเกิดอันตรายต่อตนเอง ผู้อื่น และทรัพย์สิน ต้องจัดการภายใน 2ชั่วโมง' ;
            }
            else if(tmp_score == 1) {
                text_result = 'ผู้ป่วยมีพฤติกรรมก้าวร้าวรุนแรงที่ต้องจัดการภายใน 24ชั่วโมง' ;
            }            
            else {
                text_result = '-' ;
            }
        } else {
            text_result =  '-';
        }
        return text_result;
    }
}

